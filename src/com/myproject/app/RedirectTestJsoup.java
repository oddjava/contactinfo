package com.myproject.app;

import java.io.IOException;
import java.net.URL;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;

public class RedirectTestJsoup 
{
	static String baseUrl;
	static int counter=0;


	public static void main(String[] args) 
	{
		//String originalURL = "http://www.grimesiowa.com/";
		String originalURL = "https://www.zomato.com";
		
		RedirectTestJsoup redirectTestJsoup=new RedirectTestJsoup();
		String redirectedURL=redirectTestJsoup.getRedirectedURL(originalURL);
		System.out.println("Redirected URL::: "+redirectedURL);
		
	}
	
	public String getRedirectedURL(String originalURL) // connection of url using Jsoup
	{
		baseUrl=originalURL;
		String redirectedURL =checkRedirectingURL(originalURL).url().toString();	
		return redirectedURL;
	}
	
	static Response checkRedirectingURL(String url) //redirecting until gets right link
	{
		String temp1="";
		String temp2="";
		Response response=null;
		int redirectResponseStatus=0;
		
		try 
		{
			response = Jsoup.connect(url).header("Accept-Language", "en").timeout(30000).followRedirects(false).execute();
			redirectResponseStatus=response.statusCode();
			
			//System.out.println(response.statusCode() + " : " + url);
			
			//System.out.println( "Resonse Code: " + redirectResponseStatus);  // if link redirected  to another link
			if(redirectResponseStatus==300||redirectResponseStatus==301||redirectResponseStatus==303||
			   redirectResponseStatus==304||redirectResponseStatus==305||redirectResponseStatus==306||redirectResponseStatus==307||redirectResponseStatus==308)
			{
			if(counter==8)
			{
				counter=0;
				response=null;
				return response;
			}	
			if (response.hasHeader("location")) 
			{
				String redirectUrl = response.header("location");
				//System.out.println("Redirected="+redirectUrl);
				if(redirectUrl.trim().startsWith("/"))
				{
					if(baseUrl.endsWith("/"))
					{
						counter++;
						response=checkRedirectingURL(baseUrl.substring(0, baseUrl.length()-1)+redirectUrl);
					}
					else
					{
						counter++;
						response=checkRedirectingURL(baseUrl+redirectUrl);
					}
				}
				else if(redirectUrl.trim().startsWith("http"))
				{
					temp1=new URL(baseUrl).getHost().replace("www.", "").replace(".com", "");
					temp2=new URL(redirectUrl).getHost().replace("www.", "").replace(".com", "");
							
					/*if(!(temp2.toLowerCase().contains(temp1.toLowerCase())))
					{
						System.out.println("Differnt Host=> "+temp1+"\t"+temp2);
						response=null;
						return response;
					}
					else*/
					{	
						counter++;
						response=checkRedirectingURL(redirectUrl);																	
					}
					
				}
				else
				{	counter++;
					response=checkRedirectingURL(baseUrl+"/"+redirectUrl);
				}			
			}
				return response;
			}
			else   // link is not redirected
			{
				return response;
			}
		} 		
		catch (IOException e) 
		{
			//System.out.println("In Redirect= "+e.getClass().getName()+"\t"+e.getMessage()+"\t"+url);
			return response;
		}
		catch (Exception e) 
		{
			//System.out.println("In Redirect= "+e.getClass().getName()+"\t"+e.getMessage()+"\t"+url);
			return response;
		}
		
	}

}
