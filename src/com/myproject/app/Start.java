package com.myproject.app;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;



public class Start 
{
	static MongoClient mongoClient = new MongoClient("104.251.217.179", 27017);
	static DB db =mongoClient.getDB("AppsDB");
	static DBCollection parallelDB = db.getCollection("parallelWords");	//Collection name "parallelWords"
	static DBCollection parallelTitleDB = db.getCollection("parallelTitle");

	static String fbRegex, emailRegex, linkedinRegex, twitterRegex, googleRegex,instaRegex, pinterestRegex;

	static String careerWord,pricingWord,eventWord,webinarWord,newsWord,forumWord,contactWord;
	static String careerTitle,pricingTitle,eventTitle,webinarTitle,newsTitle,forumTitle,contactTitle;
	String facebookLink="",linkedinLink="",twitterLink="",googleLink="",instagramLink="",pinterestLink="";
	String emailId="",phone="";
	String seedUrl="";
	static
	{
		fbRegex ="((http|https)://)?(www[.])?(in.)?facebook.com/.+";
		linkedinRegex = "((http|https)://)?(www[.])?(in.)?linkedin.com/.+";
		twitterRegex ="((http|https)://)?(www[.])?(in.)?twitter.com/.+";
		googleRegex = "((http|https)://)?(www[.])?plus.google.com/.+";
		instaRegex = "((http|https)://)?(www[.])?(in.)?instagram.com/.+";
		pinterestRegex ="((http|https)://)?(www[.])?(in.)?pinterest.com/.+";
		emailRegex="[a-zA-Z0-9_.+-:]+ ?@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+";

		careerWord = getParallelWords("career");
		pricingWord =getParallelWords("pricing");
		eventWord=getParallelWords("event");
		webinarWord=getParallelWords("webinar");
		newsWord=getParallelWords("news");
		forumWord=getParallelWords("forum");
		contactWord=getParallelWords("contact");

		careerTitle=getParallelTitles("career");
		pricingTitle=getParallelTitles("pricing");
		eventTitle=getParallelTitles("event");
		webinarTitle=getParallelTitles("webinar");
		newsTitle=getParallelTitles("news");
		forumTitle=getParallelTitles("forum");
		contactTitle=getParallelTitles("contact");

		//System.out.println("contact words : "+contactWord);

	}
	public static void main(String argd[]) throws IOException
	{
		String url="http://www.sayajihotels.com/";

		Document document;
		Start startObj = new Start();
		Map<String,String> socailLinksByUrl = startObj.getSocailLinksByUrl(url);
		System.out.println("socailLinksByUrl : "+socailLinksByUrl);
		document=Jsoup.connect(url).timeout(10000).get();
		//System.out.println("doc : "+document);
		Map<String,String> socailLinksByDoc = startObj.getSocailLinksByDoc(document);
		System.out.println("socailLinksByDoc : "+socailLinksByDoc);
		 
		String emailByUrl =startObj.getEmailByUrlwithCompany(url);
		System.out.println("emailByUrlwith Company name check : "+emailByUrl);
		//document=Jsoup.connect(url).timeout(20000).get();

		String emailByUrlwithoutCompany =startObj.getEmailByUrlwithoutCompany(url);
		System.out.println("emailByUrl without Company name check: "+emailByUrlwithoutCompany);
		document=Jsoup.connect(url)
				.userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
				.referrer("http://www.google.com")              
				.get();

		String emailByDoc =startObj.getEmailByDoc(document);
		System.out.println("emailByDoc : "+emailByDoc);

		String phoneByUrl = startObj.getPhoneByUrl(url);
		System.out.println("phoneByUrl : "+phoneByUrl);
		document=Jsoup.connect(url).get();
		String phoneByDoc = startObj.getPhoneByDoc(document);
		System.out.println("phoneByDoc : "+phoneByDoc);

		Map<String,String> allLinksByUrl =startObj.getAllTypeLinksByUrl(url);
		System.out.println("allLinksByUrl : "+allLinksByUrl);
		document=Jsoup.connect(url).get();
		Map<String,String> allLinksByDoc =startObj.getAllTypeLinksByDoc(document);
		System.out.println("allLinksByDoc : "+allLinksByDoc);
	}

	public Map<String,String> getAllTypeLinksByUrl(String url)
	{
		Map<String,String> bean= new HashMap<String,String>();
		GetData getData = new GetData();
		try
		{
			Document document = getData.getJsoupDoc(url);
			bean=getAllTypeLinks(document);
		}catch(Exception e){}

		return bean;
	}

	public Map<String,String> getAllTypeLinksByDoc(Document document)
	{
		Map<String,String> bean= new HashMap<String,String>();
		bean=getAllTypeLinks(document);
		return bean;
	}
	public Map<String,String> getAllTypeLinks(Document document)
	{
		Map<String,String> bean= new HashMap<String,String>();
		String webinarLink ="",eventLink="",forumLink="",careerLink="",pricingLink="",newsLink="";
		GetData getData = new GetData();

		try
		{

			careerLink=getData.getTypeLink(document, careerWord, careerTitle);
			pricingLink=getData.getTypeLink(document,pricingWord,pricingTitle);
			eventLink=getData.getTypeLink(document,eventWord,eventTitle);
			webinarLink=getData.getTypeLink(document,webinarWord,webinarTitle);
			newsLink=getData.getTypeLink(document, newsWord, newsTitle);
			forumLink=getData.getTypeLink(document,forumWord,forumTitle);

			bean.put("careerLink",careerLink);
			bean.put("pricingLink",pricingLink);
			bean.put("eventLink",eventLink);
			bean.put("webinarLink",webinarLink);
			bean.put("newsLink",newsLink);
			bean.put("forumLink",forumLink);

		}
		catch(Exception e){e.printStackTrace();}
		return bean;	
	}


	public Map<String,String> getSocailLinksByUrl(String url)
	{
		Map<String,String> bean= new HashMap<String,String>();
		GetData getData = new GetData();
		try
		{
			Document document = getData.getJsoupDoc(url);
			bean=getSocailLinks(document);
		}catch(Exception e){}

		return bean;
	}
	public Map<String,String> getSocailLinksByDoc(Document document)
	{
		Map<String,String> bean= new HashMap<String,String>();
		bean=getSocailLinks(document);
		return bean;
	}
	public Map<String,String> getSocailLinks(Document document)
	{
		ExecutorService executor;
		Future<?> future;
		GetData getData = new GetData();




		Map<String,String> bean= new HashMap<String,String>();
		Document doc;
		String textProcess="";

		try{

			//fetching all social links from home page

			facebookLink=getData.getSocialLink(document,fbRegex);
			twitterLink=getData.getSocialLink(document,twitterRegex);
			googleLink=getData.getSocialLink(document,googleRegex);
			linkedinLink=getData.getSocialLink(document,linkedinRegex);
			instagramLink=getData.getSocialLink(document,instaRegex);
			pinterestLink=getData.getSocialLink(document,pinterestRegex);



			seedUrl="";
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
			{
				seedUrl = getData.getTypeLink(document,contactWord,contactTitle); 
			}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}

			System.out.println("contact link : "+seedUrl);
			//companyName = getData.getCompany(url);
			//if we didn;t get social links on home page then go to contact page

			if(!seedUrl.equals("")) 
			{
				doc= getData.getJsoupDoc(seedUrl);

				//System.out.println("doc : "+doc);
				if(facebookLink.equals(""))
					facebookLink=getData.getSocialLink(doc,fbRegex);

				if(twitterLink.equals(""))
					twitterLink=getData.getSocialLink(doc,twitterRegex);

				if(googleLink.equals(""))
					googleLink=getData.getSocialLink(doc,googleRegex);

				if(linkedinLink.equals(""))
					linkedinLink=getData.getSocialLink(doc,linkedinRegex);

				if(instagramLink.equals(""))
					instagramLink=getData.getSocialLink(doc,instaRegex);

				if(pinterestLink.equals(""))
					pinterestLink=getData.getSocialLink(doc,pinterestRegex);

			}


		}
		catch(Exception e)
		{
			System.out.println("Link is not supported by system........");
			e.printStackTrace();
			//e.printStackTrace();
		}

		bean.put("facebookLink",facebookLink);
		bean.put("twitterLink",twitterLink);
		bean.put("googleLink",googleLink);
		bean.put("linkedinLink",linkedinLink);
		bean.put("instagramLink",instagramLink);
		bean.put("pinterestLink",pinterestLink);

		return bean;

	}

	public String getEmailByUrlwithCompany(String url)
	{
		String email="";
		GetData getData = new GetData();
		try
		{
			String companyName=getData.getCompany(url);
			Document document = getData.getJsoupDoc(url);
			email=getEmail(document,companyName);
		}catch(Exception e){}
		return email;
	}

	public String getEmailByUrlwithoutCompany(String url)
	{
		String email="",companyName="";
		GetData getData = new GetData();
		try
		{
			Document document = getData.getJsoupDoc(url);
			email=getEmail(document,companyName);
		}catch(Exception e){}
		return email;
	}
	public String getEmailByDoc(Document document)
	{
		String companyName="";
		String email=getEmail(document,companyName);		
		return email;
	}
	public String getEmail(Document document,String companyName)
	{
		String emailId="";
		Set<String> emailSet = new HashSet<String>();
		Set<String> emailSet1 = new HashSet<String>();
		ExecutorService executor;
		Future<?> future;
		GetData getData = new GetData();
		Document doc=null;

		String textProcess="";
		try{


			seedUrl="";
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
			{
				seedUrl = getData.getTypeLink(document,contactWord,contactTitle); 
			}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}

			if(!seedUrl.equals(""))  // if contact Page found
			{
				doc=getData.getJsoupDoc(seedUrl);


				textProcess = getData.getBoiler(doc);
				//System.out.println("HIIIIII"+textProcess);
				textProcess=textProcess.replace("[at]", "@").replace("(at)", "@").replace(" @ ", "@");
				emailSet=getData.getEmailId(textProcess,companyName,doc);

			}
			//System.out.println("emailid"+emailId);
				textProcess = getData.getBoiler(document);
				textProcess=textProcess.replace("[at]", "@").replace("(at)", "@").replace(" @ ", "@");

				emailSet1=getData.getEmailId(textProcess,companyName,document);

			

			emailSet.addAll(emailSet1);
			
			if(!emailSet.isEmpty()) //if emailId is present
			{
				for(String e1:emailSet)
				{

					if(!e1.equals("")) // checking emailID if it null
					{
						emailId+=e1+",";
						//System.out.println("inside"+emailId);
					}
				}
				if(!emailId.isEmpty()){
					emailId = emailId.substring(0,emailId.length()-1); //Remove the last comma(,)
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Link is not supported by system........");
		}
		return emailId;
	}

	public String getPhoneByUrl(String url)
	{

		GetData getData = new GetData();
		Document document = getData.getJsoupDoc(url);
		String phone=getPhone(document);
		return phone;
	}

	public String getPhoneByDoc(Document document)
	{
		String phone=getPhone(document);
		return phone;
	}
	public String getPhone(Document document)
	{
		ExecutorService executor;
		Future<?> future;
		GetData getData = new GetData();
		Document doc;
		String textProcess="";
		try{

			seedUrl="";
			executor = Executors.newFixedThreadPool(1);   //set max limit to a task
			future = executor.submit(new Runnable() 
			{	@Override
				public void run() 
			{
				seedUrl = getData.getTypeLink(document,contactWord,contactTitle); 
			}
			});
			executor.shutdown();            //        <-- reject all further submissions
			try 
			{
				future.get(10, TimeUnit.SECONDS);  //     <-- wait 8 seconds to finish
			} 
			catch (Exception e) 
			{    //     <-- possible error cases
				System.out.println("ContactInfo interrupted");
			}

			if(!seedUrl.equals(""))  // if contact Page found
			{
				doc=getData.getJsoupDoc(seedUrl);

				textProcess = getData.getBoiler(doc);
				phone=getData.getPhone(textProcess);
			}
			//System.out.println("emailid"+emailId);
			if(phone.equals(""))
			{
				textProcess = getData.getBoiler(document);

				phone=getData.getPhone(textProcess);
			}


		}
		catch(Exception e)
		{
			System.out.println("Link is not supported by system........");
			//e.printStackTrace();
		}

		return phone;
	}



	public static String getParallelWords(String type) // fetching parallel Words according to type
	{

		String parallelWord ="";
		DBCursor cursor = parallelDB.find(new BasicDBObject("type",type));
		if(cursor.hasNext())
		{
			DBObject present = cursor.next();

			parallelWord= present.get("parallelWords").toString();
			return parallelWord;
		}
		return parallelWord;
	}

	public static String getParallelTitles(String type) { // fetching parallel Titles according to Type
		String parallelTitles ="";
		DBCursor cursor = parallelTitleDB.find(new BasicDBObject("type",type));
		if(cursor.hasNext())
		{
			DBObject present = cursor.next();
			parallelTitles= present.get("parallelTitles").toString();
			return parallelTitles;
		}
		return parallelTitles;

	}



}
